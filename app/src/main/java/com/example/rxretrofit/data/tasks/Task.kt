package com.example.rxretrofit.data.tasks

import com.google.gson.annotations.SerializedName

data class Task(
    @SerializedName("userId") var userId: Int
    , @SerializedName("id") var id: Int
    , @SerializedName("title") var title: String
    , @SerializedName("completed") var isCompleted: Boolean)