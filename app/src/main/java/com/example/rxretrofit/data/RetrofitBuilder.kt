package com.example.rxretrofit.data

import com.example.rxretrofit.data.tasks.Task
import com.example.rxretrofit.interfaces.JSonPlaceHolderApi
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class RetrofitBuilder {

    @Singleton
    @Provides
    fun getRetrofit(): JSonPlaceHolderApi {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(JSonPlaceHolderApi::class.java)
    }
}