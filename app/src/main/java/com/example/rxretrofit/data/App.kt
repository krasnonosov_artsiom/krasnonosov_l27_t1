package com.example.rxretrofit.data

import android.app.Application
import com.example.rxretrofit.interfaces.AppComponent
import com.example.rxretrofit.interfaces.DaggerAppComponent

class App: Application() {

    companion object {
        val component: AppComponent by lazy { DaggerAppComponent.create() }
    }
}