package com.example.rxretrofit.data.user

import com.google.gson.annotations.SerializedName

data class Address(@SerializedName("street") var street: String,
                   @SerializedName("suite") var suite: String,
                   @SerializedName("city") var city: String,
                   @SerializedName("zipcode") var zipCode: String,
                   @SerializedName("geo") var geo: Geo)