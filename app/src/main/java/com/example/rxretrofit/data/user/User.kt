package com.example.rxretrofit.data.user

import com.google.gson.annotations.SerializedName

data class User( @SerializedName("id") var userId: Int,
                 @SerializedName("name") var userName: String,
                 @SerializedName("username") var login: String,
                 @SerializedName("email") var email: String,
                 @SerializedName("address") var adress: Address,
                 @SerializedName("phone") var phone: String,
                 @SerializedName("website") var website: String,
                 @SerializedName("company") var company: Company)