package com.example.rxretrofit.interfaces

import com.example.rxretrofit.data.tasks.Task
import com.example.rxretrofit.data.user.User
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface JSonPlaceHolderApi {

    @GET("/todos/")
    fun getTasks(): Observable<List<Task>>

    @GET("/users/{id}")
    fun getUser(@Path("id") id: Int): Observable<User>

}