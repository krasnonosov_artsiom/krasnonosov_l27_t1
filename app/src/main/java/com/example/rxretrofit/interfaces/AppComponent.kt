package com.example.rxretrofit.interfaces

import com.example.rxretrofit.data.RetrofitBuilder
import com.example.rxretrofit.data.tasks.Task
import com.example.rxretrofit.data.user.User
import dagger.Component
import io.reactivex.Observable
import javax.inject.Singleton

@Singleton
@Component(modules = [RetrofitBuilder::class])
interface AppComponent {

    fun getNetworkService(): JSonPlaceHolderApi

}