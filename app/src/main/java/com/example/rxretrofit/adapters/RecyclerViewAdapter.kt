package com.example.rxretrofit.adapters

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.rxretrofit.R
import com.example.rxretrofit.data.tasks.Task
import com.example.rxretrofit.ui.useractivity.UserActivity
import com.example.rxretrofit.utils.ColorCreator
import com.example.rxretrofit.utils.KEY_USER_ID
import com.google.android.material.chip.Chip

class RecyclerViewAdapter :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    var list: ArrayList<Task> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private val colorCreator = ColorCreator()

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView = itemView.findViewById<ImageView>(R.id.cardImageView)
        private val id = itemView.findViewById<TextView>(R.id.cardIdTextView)
        private val title = itemView.findViewById<TextView>(R.id.titleTextView)
        private val userId = itemView.findViewById<TextView>(R.id.userIdTextView)
        val chip: Chip = itemView.findViewById(R.id.chip)

        fun bind(task: Task) {
            imageView.setColorFilter(Color.parseColor(colorCreator.generateColor()))
            id.text = task.id.toString()
            title.text = task.title
            userId.text = "Assignee: User${task.userId}"

            if (task.isCompleted) {
                setChipChecked()
            } else {
                setChipUnchecked()
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, UserActivity::class.java)
                intent.putExtra(KEY_USER_ID, task.userId)
                itemView.context.startActivity(intent)
            }
        }

        fun setChipChecked() {
            chip.setChipBackgroundColorResource(R.color.colorChipTrue)
            chip.text = chip.context.resources.getString(R.string.done)
            chip.isChecked = true
        }

        fun setChipUnchecked() {
            chip.setChipBackgroundColorResource(R.color.colorChipFalse)
            chip.text = chip.context.resources.getString(R.string.todo)
            chip.isChecked = false
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.task_card_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position])
        holder.chip.setOnClickListener {
            list[position].isCompleted = !list[position].isCompleted
            if (list[position].isCompleted) {
                holder.setChipChecked()
            } else {
                holder.setChipUnchecked()
            }
        }
    }

}