package com.example.rxretrofit.utils

import java.lang.StringBuilder
import java.util.ArrayList

class ColorCreator {

    private val colors = listOf(
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B",
        "C", "D", "E", "F"
    )

    fun generateColor(): String {
        val builder = StringBuilder("#")
        for (i in 0..5) {
            val randomIndex = (colors.indices).random()
            builder.append(colors[randomIndex])
        }
        return builder.toString()
    }

    fun generateColor(amount: Int): ArrayList<String> {
        val colorsList = ArrayList<String>()
        for (i in 1..amount) {
            val builder = StringBuilder("#")
            for (n in 0..5) {
                val randomIndex = (colors.indices).random()
                builder.append(colors[randomIndex])
            }
            colorsList.add(builder.toString())
        }
        return colorsList
    }

}