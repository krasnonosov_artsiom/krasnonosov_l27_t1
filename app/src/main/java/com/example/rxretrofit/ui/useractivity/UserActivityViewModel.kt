package com.example.rxretrofit.ui.useractivity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.rxretrofit.data.App
import com.example.rxretrofit.data.user.User
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class UserActivityViewModel(application: Application, var userId: Int) : AndroidViewModel(application) {

    val liveDataUser: MutableLiveData<User> by lazy { MutableLiveData<User>() }

    fun observeUser() {
        App.component.getNetworkService()
            .getUser(userId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                liveDataUser.value = it
            }
    }



}