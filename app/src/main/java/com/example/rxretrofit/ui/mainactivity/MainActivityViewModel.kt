package com.example.rxretrofit.ui.mainactivity

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.rxretrofit.data.App
import com.example.rxretrofit.data.tasks.Task
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class MainActivityViewModel: ViewModel(){

    val recyclerViewLiveData: MutableLiveData<ArrayList<Task>> by lazy {
        MutableLiveData<ArrayList<Task>>() }

    private fun getObservableData(): Observable<List<Task>> {
        return App.component.getNetworkService().getTasks().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .retryWhen { t -> t.delay(5, TimeUnit.SECONDS) }
    }

    fun displayRequestResult() {
        getObservableData()
            .subscribe {
                recyclerViewLiveData.value = it as ArrayList<Task>
            }
    }

}