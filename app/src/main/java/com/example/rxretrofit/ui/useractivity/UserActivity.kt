package com.example.rxretrofit.ui.useractivity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.rxretrofit.R
import com.example.rxretrofit.utils.KEY_USER_ID
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity : AppCompatActivity() {

    private val userId: Int by lazy { intent.getIntExtra(KEY_USER_ID, -1) }
    private val userActivityViewModel: UserActivityViewModel by lazy { UserActivityViewModel(application, userId) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        userActivityViewModel.observeUser()
        userActivityViewModel.liveDataUser.observe(this
            , Observer { user ->
                userIdTextView.text = "${resources.getString(R.string.user_id)} ${user.userId}"
                userNameTextView.text = user.userName
                userLoginTextView.text = "${resources.getString(R.string.login)} ${user.login}"
                phoneTextView.text = "${resources.getString(R.string.phone)} ${user.phone}"
                userEmailTextView.text = "${resources.getString(R.string.email)} ${user.email}"
                streetTextView.text = user.adress.street
                suiteTextView.text = user.adress.suite
                cityTextView.text = user.adress.city
                zipCodeTextView.text = user.adress.zipCode
                val lng = user.adress.geo.lng
                val lat = user.adress.geo.lat
                latTextView.text = if (lat.startsWith("-")) lat else "+$lat"
                lngTextView.text = if (lng.startsWith("-")) lng else "+$lng"
                websiteTextView.text = "${user.website}"
                companyNameTextView.text = user.company.companyName
                catchPhraseTextView.text = user.company.catchPhrase
                bsTextView.text = user.company.bs
            })

    }

}