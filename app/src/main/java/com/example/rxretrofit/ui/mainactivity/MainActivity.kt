package com.example.rxretrofit.ui.mainactivity

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rxretrofit.R
import com.example.rxretrofit.adapters.RecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val mainActivityViewModel: MainActivityViewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }
    private val adapter by lazy { RecyclerViewAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        observeData()
    }


    private fun setupViews() {
        swipeRefresh.setOnRefreshListener {
            observeData()
            swipeRefresh.isRefreshing = false
        }
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter
    }

    private fun observeData() {
        checkInternetConnection()
        mainActivityViewModel.displayRequestResult()
        mainActivityViewModel.recyclerViewLiveData.observe(this
            , Observer { t -> adapter.list = t })
    }

    private fun checkInternetConnection() {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (!connectivityManager.isActiveNetworkMetered) {
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show()
        }
    }

}